let item = []

document.addEventListener("dragstart", function (e) {  
    item = (e.target);
});
document.addEventListener("dragover", function (e) {  
    e.preventDefault();
});
document.addEventListener("drop", function (e) {  
    if (e.target.getAttribute("data-draggable") == "target") {
        e.preventDefault(e);
        (e.target.appendChild(item));
    }
});
document.addEventListener("dragend", function () {  
    item = null;

})